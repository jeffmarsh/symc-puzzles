#!/usr/bin/env node

const workshopper = require('workshopper'),
	path        = require('path')

function fpath (f) {
	return path.join(__dirname, f)
}

workshopper({
	name        : 'SYMC-puzzels',
	title       : 'SYMC puzzels',
	subtitle    : 'Fun writing JS code to solve hard-ish problems',
	appDir      : __dirname,
	menuItems   : [],
	exerciseDir : fpath('./exercises/')
})