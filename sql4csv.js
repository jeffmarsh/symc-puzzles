/**
 * Created by jeff_marsh on 11/20/14.
 */
var fs = require('fs');

//var sql = "select FirstName, LastName, Age from test1.csv where Age='28'";

var sql = process.argv[2];

//console.log(__dirname);


//1. get file from sql
var filename = __dirname+"/exercises/sql_for_csv/data/"+sql.substring(sql.indexOf(" from ")+6, sql.indexOf(" where ")).trim();

fs.readFile(filename, function (err, data) {

	// 1:  Read the file and make arrays put of it

	if (err) throw err;
	var rows = data.toString().split("\n");
	var numCol = rows[0].split(", ").length;
	var rowArray = [];
	for (var colCnt = 0; colCnt < rows.length; colCnt++){
		rowArray.push(rows[colCnt].split(", "));
	}

	// 2: Break up the select

	var what = sql.substring(0, sql.indexOf(' from ')).split("select ")[1].split(",").map(Function.prototype.call, String.prototype.trim);
	var whereStr = sql.substring(sql.indexOf(' where ')+7);	//
	var operator = whereStr.indexOf("and") > -1 ? "and" : "or";
	var where = whereStr.split("and").map(Function.prototype.call, String.prototype.trim);

	var whereCol = [];
	var whereVal = [];

	for (var a=0; a< where.length; a++){
		var ww = where[a].split("=");
		whereCol.push(ww[0]);
		whereVal.push(ww[1].replace(/["']/g, ""));
	}
	var whatAndWhere = what.concat(whereCol);


	// 3: Loop over rows and return the fields in the what

	var results = [];
	var resultLine = [];

	for (var rows=1; rows<rowArray.length; rows++){
		for(var c=0; c < whereVal.length; c++){
			if(whereVal[c] === rowArray[rows][rowArray[0].indexOf(whereCol[c])]){
				for (var allwhat =0 ;allwhat < what.length; allwhat++){
					resultLine.push(rowArray[rows][rowArray[0].indexOf(what[allwhat])]);
				}
			}
		}
		if(typeof(resultLine[0])!= "undefined" ){
			results.push(resultLine);
			resultLine=[];
		}

	}

	console.log(results);

	//console.log(rowArray[0][1])


});