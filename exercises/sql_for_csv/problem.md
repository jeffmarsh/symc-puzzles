# The Idea
Lets say you had a need to request data from a database of users using a simple SQL select statement. However, that database doesn't actualy exist, and for whatever reason, you can't install MySQL or the like on your server. Instead you have a comma separated values document (CSV file).

So, you need to write a program that will:
 - Take an SQL select statement from the command line
 - Find the relevant data
 - Return the results as an array.

## Example:
Here is the data in my csv file named "names.csv"
Name, Age, Email
Bob, 38, bob@some_domain.com
Kate, 40, kate121@me.com
Tom, 23, tom@gmail.com
Cindy, 23, cindy@yahoo.com


Now, I want to select everyone who has an age of 23, so I would pass this to the program:

	node sql4csv.js "SELECT Name, Email FROM names.csv WHERE Age='23'"

Which would return:

	[
		['Tom', 'tom@gmail.com'],
		['Cindy', 'cindy@yahoo.com']
	]

# Specific information
For this first puzzle, the only SQL statement workshopper will send is "SELECT FirstName, LastName, Age FROM people.csv where Age='28'"

We have included the people.csv file for you. You should be able to find it at exercises/sql_for_csv/data/people.csv. So, you will probably want to reference:

	__dirname+"/exercises/sql_for_csv/data/"

## Please remember, once you have completed this exercise:
 - Send your source code to your Symantec representative
 - Note how much time you spent coming up with this solution.

Thanks for taking the time and we hope you have fun!